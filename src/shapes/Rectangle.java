package shapes;

import shapes.Interface.IResizeable;

public class Rectangle extends Shape implements IResizeable {
	// fields
	protected float width;
	protected float height;

	public Rectangle() {
		super("Rectangle");
		this.width = 0;
		this.height = 0;
		// these are the method in Rectangle
		// once we placed update we don't need to constantly type those two out

		update();

	}

	public Rectangle(float width, float height) {
		super("Rectangle");
		this.width = width;
		this.height = height;
		// these are the method in Rectangle
		// once we placed update we don't need to constantly type those two out
		update();
	}

	@Override
	protected void computeArea() {
		this.area = this.width * this.height;

	}

	@Override
	protected void computePerimeter() {
		this.parimeter = (2 * this.width) + (2 * this.height);

	}

	private void update() {
		this.computeArea();
		this.computePerimeter();

	}

	@Override
	public void printInfo() {
		System.out.println("Rectangle Width: \t" + width);
		System.out.println("Rectangle Height:\t" + height);
		System.out.println("Rectangle Area:\t" + area);

	}

	// these two method must be implemented because of the interface
	@Override
	public void upScale(int factor) {
		// multiply to enlarge
		this.height = this.height * factor;
		this.width = this.width * factor;
		update();

	}

	@Override
	public void downScale(int factor) {
		// divide to downscale
		this.height = this.height / factor;
		this.width = this.width / factor;
		update();

	}

	
}
