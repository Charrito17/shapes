package shapes.Interface;

public interface IResizeable {
	
	void upScale(int factor);
	void downScale(int factor);

}
