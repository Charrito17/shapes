package shapes.Interface;

import shapes.Shape;

public interface IComparable {

	public int compareToByArea(Shape Azim, Shape Alex);

}