package shapes;

import shapes.Interface.IComparable;

public abstract class Shape implements IComparable {
	// fields
	protected String name;
	protected float area;
	protected float parimeter;

	public Shape() {
		this.name = "";
		this.area = 0;
		this.parimeter = 0;

	}

	public Shape(String name) {
		this.name = name;
		this.area = 0;
		this.parimeter = 0;
	}

	// this right here is abstract material
	protected abstract void computeArea();

	protected abstract void computePerimeter();

	@Override
	public int compareToByArea(Shape Azim, Shape Alex) {
		
		return 0;
	}

	public void printInfo() {
		System.out.println("Name:\t" + name);
		System.out.println("Area:\t" + area);
		System.out.println("Parimeter:\t" + parimeter);
	}

}
