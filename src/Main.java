import shapes.Rectangle;
import shapes.Shape;
import shapes.Interface.IResizeable;

public class Main {
	public static void main(String[] args) {
		Shape Tony = new Rectangle(23, 2);
		Tony.printInfo();
		((Rectangle) Tony).upScale(2);
		Tony.printInfo();
		System.out.println("-------This is the Enlargeed or Downsize---------");
		IResizeable Dawit = new Rectangle(12, 12);
		((Rectangle) Dawit).printInfo();
		Dawit.upScale(2);
		((Rectangle) Dawit).printInfo();
		System.out.println("-------This is the New Shape----------------------");
		Shape Yassin = new Rectangle(20,12);
		Yassin.printInfo();

	}

}
